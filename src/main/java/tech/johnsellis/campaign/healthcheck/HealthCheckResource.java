package tech.johnsellis.campaign.healthcheck;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class HealthCheckResource implements HealthIndicator {

	@Override
	public Health health() {
		try {
			return Health.up().build();
		} catch (Exception e) {
			return Health.down(e).build();
		}
	}

}
