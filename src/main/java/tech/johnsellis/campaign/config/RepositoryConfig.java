package tech.johnsellis.campaign.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = { "tech.johnsellis.campaign.domain" })
@EnableJpaRepositories(basePackages = { "tech.johnsellis.campaign.repository" })
@EnableTransactionManagement
public class RepositoryConfig {

}