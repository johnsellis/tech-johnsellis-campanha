package tech.johnsellis.campaign.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import tech.johnsellis.campaign.domain.Campaign;
import tech.johnsellis.campaign.repository.CampaignRepository;

/**
 *
 * @author John Sellis
 */
@Service
public class CampaignService {

	@Autowired
	private CampaignRepository campaignRepository;

	public Campaign getCampaign(Long id) {
		Assert.notNull(id, "Nc não pode ser null");
		Campaign campaign = campaignRepository.findOne(id);
		return campaign;
	}

	public List<Campaign> getCampaigns() {
		return campaignRepository.findAll();
	}

	public Campaign createCampaign(Campaign campaign) {
		Campaign byEndDate = campaignRepository.retrieveByEndDate(campaign.getEndDate());
		List<Campaign> conflictEndDate = new ArrayList<>();
		while (byEndDate != null) {
			Calendar cal = byEndDate.getEndDate(); // get calendar
			cal.add(Calendar.DAY_OF_MONTH, 1); // adds one DAY
			conflictEndDate.add(byEndDate);
			byEndDate = campaignRepository.retrieveByEndDate(cal);
		}
		conflictEndDate.forEach(campaignRepository::save);
		Campaign savedCampaign = campaignRepository.saveAndFlush(campaign);
		return savedCampaign;
	}

}
