package tech.johnsellis.campaign.repository;

import java.util.Calendar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tech.johnsellis.campaign.domain.Campaign;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long> {
	
	@Query("SELECT c FROM Campaign c WHERE c.endDate = :endDate")
	Campaign retrieveByEndDate(@Param("endDate") Calendar endDate);

}
