package tech.johnsellis.campaign.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.johnsellis.campaign.domain.Campaign;
import tech.johnsellis.campaign.service.CampaignService;

/**
 * Controller Campaign
 *
 * @author John Sellis
 */
@RestController
@RequestMapping("/api")
@Api(value = "Campaign Endpoints")
public class CampaignController {

	private static final Log LOG = LogFactory.getLog("METRIC");

	@Autowired
	private CampaignService campaignService;

	@RequestMapping(value = "/campaign/{campaignId}", method = RequestMethod.GET, //
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@ApiOperation(value = "GET", notes = "Get Campaign by its Id", response = Campaign.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Campaign structure"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public Campaign getCampaignById(@PathVariable("campaignId") final Long campaignId) {
		LOG.info("# Endpoint /campaign/{campaignId}");
		Campaign campaign = campaignService.getCampaign(campaignId);
		return campaign;
	}

	@RequestMapping(value = "/campaign", method = RequestMethod.GET, //
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@ApiOperation(value = "GET", notes = "Get Campaigns", response = Campaign.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Campaign structure"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public List<Campaign> getCampaigns() {
		LOG.info("# Endpoint /campaign");
		return campaignService.getCampaigns();
	}

	@RequestMapping(value = "/campaign", method = RequestMethod.PUT, //
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE, //
			consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@ApiOperation(value = "PUT", notes = "Create a Campaign", response = Campaign.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Campaign created"),
			@ApiResponse(code = 500, message = "Unexpected error") })
	public ResponseEntity<Campaign> putCampaign(@RequestBody Campaign campaign) {
		LOG.info("# Endpoint /campaign");
		campaign = campaignService.createCampaign(campaign);
		return ResponseEntity.status(HttpStatus.CREATED).body(campaign);
	}

}
